package org.fmi.acmeconsult.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.fmi.acmeconsult.model.security.JettyUser;

@Entity
public class Consultant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String firstName;
	
	private String lastName;
	
	private Seniority seniority;
	
	@OneToOne
	private JettyUser user;

	public Consultant(){};
	
	public Consultant(String name, String lastname, Seniority seniority, JettyUser user) {
		this.firstName = name;
		this.lastName = lastname;
		this.seniority = seniority;
		this.user = user;
	}
	
	
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Seniority getSeniority() {
		return seniority;
	}
	
	public static enum Seniority{
		JUNIOR,MIDLEVEL,SENIOR;
		
		public static String[] strValues() {
			String[] result = new String[values().length];
			for(int i=0;i<result.length;i++) {
				result[i] = values()[i].toString();
			}
			return result;
		}
	}
}
