package org.fmi.acmeconsult.model.api;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.fmi.acmeconsult.model.Consultant;
import org.fmi.acmeconsult.model.Consultant.Seniority;
import org.fmi.acmeconsult.model.security.JettyUser;
import org.fmi.acmeconsult.model.security.Role;
import org.fmi.acmeconsult.model.security.RoleConst;

public class CrudFacade {
	
	public static final CrudFacade INSTANCE = new CrudFacade();

	/**
	 * is threadsafe according to JPA2 spec
	 */
	private EntityManagerFactory factory;
	
	private CrudFacade(){
	}
	
	public void init() {
		factory = Persistence.createEntityManagerFactory("acmeconsult");
	}
	
	public void destroy() {
		factory.close();
	}
	
	
	public void createConsultant(final String userName, final String pass, final String name, final String lastName, final Seniority seniority) {
		doInNewTransaction(new DbOp<Void>() {
			public Void perform(EntityManager em) {

				JettyUser user = new JettyUser(userName, pass);
				
				Role theRole = null;
				
				@SuppressWarnings("unchecked")
				List<Role> roles = em.createQuery("SELECT r FROM Role r WHERE r.role = 'consultant'").getResultList();	
				
				if(roles.size() == 0) {
					theRole = new Role(RoleConst.ROLE_CONSULTANT);
				} else{
					theRole = roles.get(0);
				}
				
				user.addRole(theRole);
				em.persist(user);
				
				Consultant cons = new Consultant(name,lastName,seniority, user);
				em.persist(cons);
				return null;
			}
		});
	}
	
	public long getConsultantsCount() {
		return readOnlyOp(new DbOp<Long>() {
			public Long perform(EntityManager em) {
				return em.createQuery("SELECT COUNT(c) FROM Consultant c", Long.class).getSingleResult();
			}
		});
	}
	
	private <T> T doInNewTransaction(DbOp<T> op) {
		EntityManager mgr = factory.createEntityManager();
		EntityTransaction tx = null;
		try{
			tx = mgr.getTransaction();
			tx.begin();
			
			
			T result = op.perform(mgr);
			
			tx.commit();
			
			return result;
			
		}catch(RuntimeException e) {
			if(canRollback(tx)) {
				tx.rollback();
			}
			throw e;
			
		}finally {
			mgr.close();
		}
	}
	
	private <T> T readOnlyOp(DbOp<T> op) {
		EntityManager mgr = factory.createEntityManager();
		try{
			return op.perform(mgr);
		}finally {
			mgr.close();
		}
	}
	
	private boolean canRollback(EntityTransaction tx) {
		return tx!=null && tx.isActive();
	}
	
	private static interface DbOp<T> {
		public T perform(EntityManager em);
	}

	public Consultant getConsultant(final String userName) {
		return readOnlyOp(new DbOp<Consultant>() {
			@Override
			public Consultant perform(EntityManager em) {
				@SuppressWarnings("unchecked")
				List<Consultant> result =  em.createQuery("SELECT c from Consultant c WHERE c.user.username = :username").
						setParameter("username", userName).getResultList();
				return result.get(0);
			}
		});
	}
}
