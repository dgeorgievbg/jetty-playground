package org.fmi.acmeconsult.model.security;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class JettyUser {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String username;
	
	@SuppressWarnings("unused")
	private String password;
	
    @JoinTable(name="UserToRole")
	@ManyToMany(cascade=CascadeType.ALL)
	private Set<Role> roles;
	
	public JettyUser() {}
	
	public JettyUser(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}
	
	public void addRole(Role role) {
		if(roles==null) {
			roles = new HashSet<Role>();
		}
		roles.add(role);
	}
}
