package org.fmi.acmeconsult.model.security;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Role {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String role;
	
	public Role(){};
	
	public Role(String role) {
		super();
		this.role = role;
	}

	public String getRole() {
		return role;
	}

}
