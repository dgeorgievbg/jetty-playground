package org.fmi.acmeconsult.model.security;

public class RoleConst {
	public static final String ROLE_CONSULTANT="consultant";
	public static final String ROLE_CUSTOMER="customer";
}
