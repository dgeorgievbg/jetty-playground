package org.fmi.acmeconsult.util;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonWriter {
	
	private static final JsonFactory factory = new JsonFactory();
	
	public static void OK(HttpServletResponse resp, String message) {
		write(resp, "OK", message);
	}
	
	public static void OK(HttpServletResponse resp, Object data) {
		try{
			JsonGenerator json = createGenerator(resp);
			json.writeObject(data);
			json.flush();
		}catch(IOException ex) {
			throw new RuntimeException(ex);
		}	
	}
	
	public static void ERROR(HttpServletResponse resp, String message) {
		write(resp, "ERROR", message);
	}
	
	public static void ERROR(HttpServletResponse resp, List<String> messages) {
		write(resp, "ERROR", message(messages));
	}

	private static String message(List<String> messages) {
		StringBuilder result = new StringBuilder();
		for(String msg: messages) {
			result.append(msg).append("\n");
		}
		return result.toString();
	}

	private static void write(HttpServletResponse resp, String status, String message) {
		try{
			JsonGenerator json = createGenerator(resp);
			json.writeStartObject();
			json.writeStringField("status", status);
			json.writeStringField("message", message);
			json.writeEndObject();
			json.flush();
		}catch(IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	private static JsonGenerator createGenerator(HttpServletResponse resp)
			throws IOException {
		JsonGenerator result =  factory.createGenerator(resp.getWriter());
		result.setCodec(new ObjectMapper(factory));
		return result;
	}
	
}
