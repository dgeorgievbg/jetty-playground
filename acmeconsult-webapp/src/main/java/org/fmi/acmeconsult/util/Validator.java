package org.fmi.acmeconsult.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


import static java.text.MessageFormat.format;
public class Validator {

	private HttpServletRequest request;
	private final List<String> messages = new ArrayList<String>();

	public Validator(HttpServletRequest req) {
		this.request=req;
	}

	public Validator notEmpty(String paramName) {
		return notEmpty(paramName, request.getParameter(paramName));
	}

	public Validator notEmpty(String paramName, String paramValue) {
		if(paramValue==null || paramValue.isEmpty()) {
			messages.add(format("{0} must be specified", paramName));
		}
		return this;
	}

	public Validator whiteListed(String paramName, String[] allowedValues) {
		return whiteListed(paramName, request.getParameter(paramName), allowedValues);
	}
	public Validator whiteListed(String paramName, String paramValue, String[] allowedValues) {
		boolean valid = false;
		for(Object allowed: allowedValues) {
			if(allowed.equals(paramValue)) {
				valid=true;
				break;
			}
		}

		if(!valid) {
			messages.add(format("{0} is not an allowed value for {1}. valid values are: {2}", paramValue, paramName, Arrays.toString(allowedValues)));
		}

		return this;
	}

	public boolean isOk() {
		return messages.isEmpty();
	}
	
	public List<String> messages() {
		return messages;
	}

}
