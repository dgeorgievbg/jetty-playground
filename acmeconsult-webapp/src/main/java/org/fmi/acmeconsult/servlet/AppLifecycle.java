package org.fmi.acmeconsult.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.fmi.acmeconsult.model.api.CrudFacade;

@WebListener
public class AppLifecycle implements ServletContextListener {


	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
//		try{
//			ctrl = new NetworkServerControl(InetAddress.getByName("localhost"), 1529);
//		}catch(Exception ex) {
//			throw new RuntimeException(ex);
//		}
		
//        ctrl.start(new PrintWriter(System.out));
		CrudFacade.INSTANCE.init();

		 
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		CrudFacade.INSTANCE.destroy();
	}

}
