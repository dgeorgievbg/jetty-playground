package org.fmi.acmeconsult.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fmi.acmeconsult.model.Consultant;
import org.fmi.acmeconsult.model.api.CrudFacade;
import org.fmi.acmeconsult.util.JsonWriter;

@SuppressWarnings("serial")
@WebServlet("consultantDetails")
@ServletSecurity(@HttpConstraint(rolesAllowed={"consultant"}))

public class ConsultantDetailsService extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		if(req.getUserPrincipal()==null) {
			return;
		}
		String userName = req.getUserPrincipal().getName();
		System.err.println(userName);
		Consultant cons = CrudFacade.INSTANCE.getConsultant(userName);

		JsonWriter.OK(resp, cons);

		return;

	}
}
