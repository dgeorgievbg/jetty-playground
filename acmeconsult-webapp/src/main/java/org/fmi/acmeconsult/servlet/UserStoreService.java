package org.fmi.acmeconsult.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fmi.acmeconsult.model.Consultant.Seniority;
import org.fmi.acmeconsult.model.api.CrudFacade;
import org.fmi.acmeconsult.util.JsonWriter;
import org.fmi.acmeconsult.util.Validator;

@SuppressWarnings("serial")
@WebServlet("createUser/*")
public class UserStoreService extends HttpServlet {
		
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String pathInfo = req.getPathInfo();
		if(pathInfo==null) {
			pathInfo="";
		}
		
		String user = pathInfo.replaceFirst("/", "");
		
		Validator validator = new Validator(req);
		validator.notEmpty("user", user).notEmpty("password").notEmpty("firstName").notEmpty("lastName").notEmpty("seniority").whiteListed("seniority",  Seniority.strValues());

		if(!validator.isOk()) {
			JsonWriter.ERROR(resp, validator.messages());
			return;
		}
		
		String pass = req.getParameter("password");
		
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String seniorityString = req.getParameter("seniority");
		
		
		CrudFacade.INSTANCE.createConsultant(user, pass, firstName, lastName, Seniority.valueOf(seniorityString));
		
		req.getSession().invalidate();
		JsonWriter.OK(resp, "Consultant created successfully");
		
	}
}
