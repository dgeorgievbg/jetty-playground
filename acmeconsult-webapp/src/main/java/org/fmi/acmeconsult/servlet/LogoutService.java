package org.fmi.acmeconsult.servlet;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fmi.acmeconsult.util.JsonWriter;


@SuppressWarnings("serial")
@WebServlet("logout")
@ServletSecurity(@HttpConstraint(rolesAllowed={"consultant","customer"}))
public class LogoutService extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		if(req.getUserPrincipal()==null) {
			return;
		}
 		
		String userName = req.getUserPrincipal().getName();
 		req.logout();
 		
 		JsonWriter.OK(resp, MessageFormat.format("user {0} logged out", userName));
		
	}
}
